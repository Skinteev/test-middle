new Promise((resolve, reject) => {
  setTimeout(() => {
    var o = {
      name: 'Василий',
      age: 25,
      job: {
        company: 'Открытая Школа',
        title: 'Разработчик'
      },
      expertise: [
        {
          workName: "EdCrunch",
          timeInMonth: 36
        },
        {
          workName: "Баду",
          timeInMonth: 20
        },
      ]
    };
    resolve(o)
  }, 2000)
}).then(o => {
  var age = o.age;
  var yearOfBirth = 2020 - age;
  var name = o.name;
  var company = o.job.company;
  var totalE = 0;

  for (var i = 0; i < o.expertise.length; i++) {
    totalE = totalE + o.expertise[i].timeInMonth;
  }

  totalE = ~~(totalE / 12);


  if (totalE % 10 === 1 && totalE % 100 !== 11) {
    totalE = totalE + " года";
  } else if (totalE % 10 >= 2 && totalE % 10 <= 4 && (totalE % 100 < 10 || totalE % 100 >= 20)) {
    totalE = totalE + " лет";
  } else {
    totalE = totalE + " лет";
  }

  console.log(name + " сейчас работает в компании " + company + " и родился в " + yearOfBirth + " году. Его опыт работы более " + totalE);
}).catch(error => {
    console.log(error);
});